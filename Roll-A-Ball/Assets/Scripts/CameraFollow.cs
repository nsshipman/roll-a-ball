﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform playerPos;
    Vector3 difference = Vector3.zero;

	// Use this for initialization
	void Start () {
        difference = transform.position - playerPos.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = playerPos.position;
		
	}
}
